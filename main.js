const {app, BrowserWindow, ipcMain, ipcRenderer} = require('electron')
const path = require('path');
let fpsWindow = null;
let endGameWindow = null;

function createWindow() {
      fpsWindow = new BrowserWindow({
            width: 1280,
            height: 720,
            center: true,
            resizable: false,
            movable: false,
            webPreferences: {
                  nodeIntegration: true,
                  contextIsolation: false,
            }
      })
      fpsWindow.loadFile('game/game.html')
}

app.whenReady().then(() => {
      createWindow()
      app.on('activate', function () {
            if (BrowserWindow.getAllWindows().length === 0) createWindow()
      })
})

app.on('window-all-closed', function () {
      if (process.platform !== 'darwin') app.quit()
})

ipcMain.on('endGame', (event, arg) => {
      console.log('la');
      endGameWindow = new BrowserWindow({
            width: 1280,
            height: 720,
            center: true,
            resizable: false,
            movable: false,
            webPreferences: {
                  nodeIntegration: true,
                  contextIsolation: false,
            },
      })
      endGameWindow.loadFile('score/score.html');
      fpsWindow.close();
})

ipcMain.on('retry', (event, arg) => {
      fpsWindow = new BrowserWindow({
            width: 1280,
            height: 720,
            center: true,
            resizable: false,
            movable: false,
            webPreferences: {
                  nodeIntegration: true,
                  contextIsolation: false,
            }
      })
      fpsWindow.loadFile('game/game.html')
      endGameWindow.close()
})

