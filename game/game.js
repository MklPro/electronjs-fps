const {ipcRenderer} = require("electron");

let scoreDiv = document.getElementById('score');
let comboDiv = document.getElementById('combo');
let timerDiv = document.getElementById('timer');
let body = document.getElementById('body');
let imageTargetPath = "../assets/images/morty.png";
let explosionImg = "../assets/images/explosion.gif";
let score = 0;
let combo = 0;
let time = 20;

scoreDiv.insertAdjacentHTML("beforeend", '<h3 class="score"> SCORE : ' + score + '</h3>');
comboDiv.insertAdjacentHTML("beforeend", '<h3 class="combo"> COMBO : X ' + combo + '</h3>');
timerDiv.insertAdjacentHTML("beforeend", '<h3 class="combo"> TIME LEFT : ' + time + '</h3>');

function randomIntFromInterval(min, max) { // min and max included
      return Math.floor(Math.random() * (max - min + 1) + min)
}

function randomRotation(image) {
      let random = Math.random();

      if (random > 0.5) {
            image.classList.add('rotationLeft');
      } else {
            image.classList.add('rotationRight');
      }
}

function generateImage() {

      let target = document.createElement('img');
      target.setAttribute('src', imageTargetPath);
      target.setAttribute('class', 'target-image');
      target.setAttribute('id', 'target-id');

      let randomX = randomIntFromInterval(80, 580);
      let randomY = randomIntFromInterval(80, 1180);
      let randomPop = (Math.random() * (1 - 0.5) + 0.5) * 1000;
      let randomDepop = (Math.random() * (3 - 2) + 2) * 1000;

      target.style.top = randomX + 'px';
      target.style.left = randomY + 'px';
      randomRotation(target);

      body.appendChild(target);

      setTimeout(() => {
            target.remove();
      }, randomDepop)
      setTimeout(generateImage, randomPop);
}

body.addEventListener('click', ev => {
      let target = ev.target.getAttribute('id');

      if (target === 'target-id') {
            ev.target.setAttribute('src', explosionImg);
            ev.target.setAttribute('data-clicked', '1');

            setTimeout(() => {
                  ev.target.remove();
            }, 500);

            if (ev.target.hasAttribute('data-clicked')) {
                  combo++;
                  comboDiv.innerHTML = '<h3 class="combo"> COMBO : X ' + combo + '</h3>';
                  score += 1 + combo;
                  scoreDiv.innerHTML = '<h3 class="score"> SCORE : ' + score + '</h3>';
            } else {
                  combo = 0;
                  comboDiv.innerHTML = '<h3 class="combo"> COMBO : X ' + combo + '</h3>';
            }

      } else {
            combo = 0;
            comboDiv.innerHTML = '<h3 class="combo"> COMBO : X ' + combo + '</h3>';
      }
})

function timer() {
      time -= 1;
      timerDiv.innerHTML = '<h3 class="combo"> TIME LEFT : ' + time + '</h3>';

      if(time < 0 ){
            ipcRenderer.send('endGame')
      }
      setTimeout(timer, 1000);
}

function game() {
      if (time > 0) {
            generateImage();
            timer();
      }
}

game();
